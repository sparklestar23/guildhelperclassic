-- local FOLDER_NAME, private = ...
local name, GHC = ...

local addon = LibStub("AceAddon-3.0"):NewAddon(GHC.addon_name, "AceConsole-3.0", "AceComm-3.0")
addon.Db = GHC.Db
addon.Name = name
addon.LocName = select(2, GetAddOnInfo(addon.Name))
addon.Notes = select(3, GetAddOnInfo(addon.Name))
_G.GuildHelperClassic = addon


function addon:PlayerDataCommHandler(prefix, message, distribution, sender)
    self:Print(prefix, message, distribution, sender)
    GHC.Functions.ParsePlayerData(message)
end


function addon:GuildBankMoneyCommHandler(prefix, message, distribution, sender)
    self:Print(prefix, message, distribution, sender)
    GHC.UI.GuildBankParentFrame.MoneyText:SetText(GetCoinTextureString(message))
    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named since
    -- AceComm handles packetizing large messages automatically
    GHC.Functions.GuildBankStart(message)
end


function addon:GuildBankDataCommHandler(prefix, message, distribution, sender)
    self:Print(prefix, message, distribution, sender)
    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named since
    -- AceComm handles packetizing large messages automatically
    GHC.Functions.ParseGuildBankData(message)
end


function addon:GuildBankFinishCommHandler(prefix, message, distribution, sender)
    self:Print(prefix, message, distribution, sender)
    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named
    GHC.Functions.GuildBankFinish()
end


function addon:UpdateMeCommHandler(prefix, message, distribution, sender)
    self:Print(prefix, message, distribution, sender)
    if GetAddOnMetadata("GuildHelperClassic", "Version") ~= message then
        self:Print("New version available!")
        GhcDb[UnitGUID("player")].NewVersion = message
    end
end


-- Default Comm handler
function addon:OnCommReceived(prefix, message, distribution, sender)
    self:Print("OnCommReceived", prefix, message, distribution, sender)
end


function addon:GUILD_ROSTER_UPDATE(eventName)
    self:Print(eventName)
    -- TODO Is this required now that the ui is self contained?
    local t = C_Timer.After(3, function() GHC.Functions.ScanGuildRoster() end)
end


function addon:PlayerDataChangedHandler(eventName)
    self:Print(eventName)
    if GhcDb[UnitGUID('player')].AutoSendCharacterData == true then
        local t = C_Timer.After(2, function() GHC.Functions.SendMyData() end)
    end    
end


function addon:ChatCommand(msg)
    if msg == nil or msg == "" then
        msg = "log"
    end
    commands = {
        ["help"] = function() self:Print("helping :/") end,
        ["open"] = function() GHC.UI.Toggle() end,
        ["fix"] = function() StaticPopup_Show("GHC_FixMe") end,
        ["scangb"] = GHC.Functions.ScanGuildBank
    }
    local prefix, nextposition = self:GetArgs(msg, 1)
    local cmd = commands[prefix]
    if cmd ~= nil then
        cmd()
    else
        local message = self:GetArgs(msg, 1, nextposition)
        if prefix == nil or message == nil then
            self:Print("GHC_Slash: cmd: nil")
        else
            self:Print("GHC_Slash: cmd:", prefix, message)
            self:SendCommMessage(prefix, message, "GUILD")
        end
    end
end


-- /steve ghc-playerdata Player-4647-00C843E1:Kuothe:PRIEST:TROLL:MALE:19:Alchemy:70:Herbalism:92:4:Kuothe

function addon:OnInitialize()
    self:RegisterChatCommand("ghc", "ChatCommand")

    self:RegisterComm("ghc-update-me", "UpdateMeCommHandler")
    self:RegisterComm("ghc-playerdata", "PlayerDataCommHandler")
    self:RegisterComm("ghc-gbdata-s", "GuildBankMoneyCommHandler")
    self:RegisterComm("ghc-gbdata", "GuildBankDataCommHandler")
    self:RegisterComm("ghc-gbdata-f", "GuildBankFinishCommHandler")
end


function addon:OnEnable()
    local t = C_Timer.After(1, function()
        GHC.UI.DrawGuildRosterListview() 
        GHC.UI.DrawGuildClassSummaryBars() 
        GHC.UI.DrawGuildBankGridView()
        GHC.Functions.Load() 
        GHC.UI.LoadNewUI() 
        GHC.UI.PlayerOptionsFrame.PlayerRoleDropdown_Init()
        GHC.UI.RosterScrollFrame.FilterProfDropDown_Init()
        GHC.UI.RosterScrollFrame.FilterRoleDropDown_Init()
    end)

    self:RegisterEvent("GUILD_ROSTER_UPDATE")
    self:RegisterEvent("CHAT_MSG_SKILL", "PlayerDataChangedHandler")
    self:RegisterEvent("PLAYER_LEVEL_UP", "PlayerDataChangedHandler")
end


function addon:OnDisable()
    -- AceEvent-3.0 automatically handles unregistering events
end