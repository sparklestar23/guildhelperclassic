## Interface: 11302
## Author: Copperbolts, Forestsage, Kasharg
## Version: 0.0.16
## Title: |cff55ae00Guild Helper Classic|r
## Notes: Guild helper addon allowing guild members to share character information. Displays guild class summary as well!
## SavedVariables: GhcDb, GhcLDBIconDb

embeds.xml

GHC_Db.lua
GHC_Scripts.lua

GHC_NewUI.xml

GHC_UI.lua

Config.lua
