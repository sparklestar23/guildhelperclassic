local name, GHC = ...

local addon = LibStub("AceAddon-3.0"):GetAddon(GHC.addon_name)

local options = {
    type = "group",
    -- get = function(info) addon:Print("get") end,
    -- set = function(info, val) addon:Print("set", val) end,
    args = {
        characterRole = {
            order = 100,
            name = "Character role",
            desc = "Select character role(s)",
            type = "select",
            get = function(info) addon:Debug("get", GhcDb[UnitGUID("player")].Role); return GhcDb[UnitGUID("player")].Role end,
            set = function(info, val) addon:Debug("set", val); GhcDb[UnitGUID("player")].Role = val; GHC.Functions.SetMyData(); GHC.Functions.SendMyData() end,
            values = {
                -- nil,
                "DPS",
                "Healer",
                "Tank",
                "DPS-Healer",
                "DPS-Tank",
                "Healer-Tank",
                "Any",
                "Guild Bank",
            }
        },
        mainCharacterName = {
            order = 110,
            name = "Main character name",
            desc = "Enter your main's name",
            type = "input",
            multiline = false,
            get = function(info) addon:Debug("get", GHC.Var.MainsName); return GHC.Var.MainsName end,
            set = function(info, val) addon:Debug("set", val); GHC.Var.MainsName = val; GHC.Functions.SetMyData(); GHC.Functions.SendMyData() end,
        },
        autoSendCharacterData = {
            order = 120,
            width = "double",
            name = "Auto send character data",
            desc = "Allowing this option will send your characters data whenever you gain a skill in a profession or level up!",
            type = "toggle",
            get = function(info) addon:Debug("get", GhcDb[UnitGUID('player')].AutoSendCharacterData); return GhcDb[UnitGUID('player')].AutoSendCharacterData end,
            set = function(info, val) addon:Debug("auto send character data:", val); GhcDb[UnitGUID('player')].AutoSendCharacterData = val end,
        }
    }
}

local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
AceConfigRegistry:RegisterOptionsTable(name, options, false)

local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local f = AceConfigDialog:AddToBlizOptions(name, GHC.addon_name, nil)