0.0.2 - 06/09/2019
bug fixed - character data now being parsed correctly
updated addon database structure
added version update system

0.0.3 - 07/09/2019
added profession icon tooltip to guild roster listview items
added slash command to run an addon settings reset option
fixed issue where database wasnt created properly
added check to guild scan so if no guild data available from game then nothing will run

0.0.4 - 08/09/2019
added guild bank system
added auto send character data sytem

0.0.5 08/09/2019
corrected guild bank scan check from 1 == 1 to if guildRank == 0 or guilRank == 1

0.0.6
started work on a new xml file for better UI, now using a tab system with resizing to follow
added sort/filter options to roster
TODO - add guild bank role option, maybe also bank alt and other?

0.0.7 19/09/2019
--the addon is becoming very messy and feels out of control, an overhaul will probably happen soon where the scripts and events are better sorted etc
added alt info option
added send bank data button
added bank money info
started work on options tab
changed frame texture to grey style

0.0.10 20/09/2019
inital work porting to Ace3
fixed send bank data button not sending start frame
fixed bank money initial display 0

0.0.11 20/09/2019
add Debug print function and remove debug prints from release

0.0.13 21/09/2019
add missing AceEvent mixin 
fix sel:Debug typos
improve /ghc help information

0.0.14 21/09/2019
use AceConfig for options management
revert to 0.0.9-beta delay for GUID and RegisterComm ordering=
ensure GuildBank and GuildBankMoney also get initialized properly along with GuildDb and GuildDbBackup
fix ChatThrottleLib embed errors

0.0.15 21/09/2019
fix scangb not serializing data, only send item.ID and .Count

0.0.16 22/09/2019
add minimap button to open/close Guild Helper Classic or open the Blizzard options frame
enable AceConfig option table validation
