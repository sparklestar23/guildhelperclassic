--[==[

Copyright ©2019 Samuel Thomas Pain

The contents of this addon, excluding third-party resources, are
copyrighted to their authors with all rights reserved.

This addon is free to use and the authors hereby grants you the following rights:

1.     You may make modifications to this addon for private use only, you
    may not publicize any portion of this addon.

2.     Do not modify the name of this addon, including the addon folders.

3.     This copyright notice shall be included in all copies or substantial
    portions of the Software.

All rights not explicitly addressed in this license are reserved by
the copyright holders.

]==]--


local name, GHC = ...

local DEBUG = false

local addon = LibStub("AceAddon-3.0"):NewAddon(GHC.addon_name, "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0", "AceSerializer-3.0")
addon.Db = GHC.Db
addon.Name = name
addon.LocName = select(2, GetAddOnInfo(addon.Name))
addon.Notes = select(3, GetAddOnInfo(addon.Name))

local tinsert, tconcat, tremove = table.insert, table.concat, table.remove

GHC.Functions = {}

GHC.Var = {
    NewVersion = '0.0.16',
    RosterSort = nil,
    ParseGuildBank = false,
    MainsName = '-',
}

function GHC_OnLoad(self)
    self:SetMovable(true)
    self:EnableMouse(true)
    self:RegisterForDrag("LeftButton")
    self:SetScript("OnDragStart", self.StartMoving)
    self:SetScript("OnDragStop", self.StopMovingOrSizing)
end


-- prints addon message to chat window using addon formatting
function GHC.Functions.PrintMessage(msg)
    addon:Print(msg)
end

--debug function to find event args
function GHC.Functions.GetArgs(...)
    for i=1, select("#", ...) do
        arg = select(i, ...)
        print(i.." "..tostring(arg))
    end
end

--set a frame to be moveable
function GHC.Functions.MakeFrameMove(frame)
    frame:SetMovable(true)
    frame:EnableMouse(true)
    frame:RegisterForDrag("LeftButton")
    frame:SetScript("OnDragStart", frame.StartMoving)
    frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end

--used as a nuclear option to wipe and restore default db
function GHC.Functions.Reset(guildName)
    if GHC.Functions.EnsureDbExists() then
        local gName, rankName, rankIndex, _ = GetGuildInfo('player')
        if gName == guildName then
            GHC.Functions.PrintMessage('found matching guild name, creating new settings')
            GhcDb = {}
            GhcDb[gName] = { GuildDb = {}, GuildDbBackUp = {}, GuildBank = {} }
            GhcDb[UnitGUID('player')] = { Role = 0, Name = UnitName('player'), CurrentVersion = GetAddOnMetadata("GuildHelperClassic", "Version"), NewVersion = GHC.Var.NewVersion }
            GHC.Functions.ScanGuildRoster()
        else
            GHC.Functions.PrintMessage('unable to find matching guild, please make sure guild name entered corectly')
            GhcDb = {}
            GhcDb[UnitGUID('player')] = { Role = 0, Name = UnitName('player'), CurrentVersion = GetAddOnMetadata("GuildHelperClassic", "Version"), NewVersion = GHC.Var.NewVersion }
        end
    else
        GHC.Functions.PrintMessage('either you are not in a guild or the guild information is not available, please try opening the guild window and run reset again.')
    end
end

--called 1 sec after addon loaded event allowing for GUIDs
function GHC.Functions.Load()
    GHC.Functions.EnsureDbExists()
    
    --create player settings db
    if GhcDb[UnitGUID('player')] == nil then
        GhcDb[UnitGUID('player')] = { Role = 0, Name = UnitName('player'), AutoSendCharacterData = true, CurrentVersion = GetAddOnMetadata("GuildHelperClassic", "Version"), NewVersion = GHC.Var.NewVersion } 
        GHC.Functions.PrintMessage('created character settings for '..UnitName('player'))
    else
        GHC.Functions.PrintMessage('loaded character settings for '..UnitName('player'))
    end

    GHC.Functions.MakeFrameMove(GHC.UI.ParentFrame)

    --required for older versions
    if GhcDb[UnitGUID('player')].AutoSendCharacterData == nil then
        GhcDb[UnitGUID('player')].AutoSendCharacterData = true
    end
    -- GHC.UI.AutoSendMyDataCheckBox:SetChecked(GhcDb[UnitGUID('player')].AutoSendCharacterData)

    if GhcDb[UnitGUID('player')] and GhcDb[UnitGUID('player')].AutoSendCharacterData == true then
        local t = C_Timer.After(10, function() GHC.Functions.SendMyData() end)
    end

    if GHC.Functions.EnsureDbExists() then
        GHC.Functions.AddonUpdater()
        local guildName, guildRankName, guildRankIndex, _ = GetGuildInfo('player')

        if GhcDb[guildName].GuildDb ~= nil then
            for k, member in pairs(GhcDb[guildName].GuildDb) do
                if member.GUID == UnitGUID('player') then
                    if member.MainsName ~= '-' then
                        GHC.Var.MainsName = member.MainsName
                        -- GHC.UI.PlayerOptionsFrame.AltInfoInput:SetText(member.MainsName)
                    end
                end
            end
        end

        if guildRankIndex == 0 or guildRankIndex == 1 then
            --GHC.UI.PlayerOptionsFrame.ScanGuildBankButton:Show() -- does this work being located in the addon when you need to have bank open?
        end

    end
    
end

function GHC.Functions.AddonUpdater()
    if GhcDb[UnitGUID('player')].NewVersion ~= GhcDb[UnitGUID('player')].CurrentVersion then
        --print(GhcDb[UnitGUID('player')].NewVersion, GhcDb[UnitGUID('player')].CurrentVersion)
        StaticPopup_Show ("GHC_UpdatePopup")
    end
end

--used for roster professions icons
function GHC.Functions.ShowTooltip(frame, text)
    GHC.UI.Tooltip:SetParent(frame)    
    GHC.UI.Tooltip.Text:SetText(text)
    local w = GHC.UI.Tooltip.Text:GetStringWidth()
    GHC.UI.Tooltip:SetSize(w + 24, 30)
    GHC.UI.Tooltip:SetPoint("TOPLEFT", - w * 1.25, 12)
    GHC.UI.Tooltip:Show()
end


-- TODO Change the entire GhcDb over to AceDB-3.0
--  https://www.wowace.com/projects/ace3/pages/api/ace-db-3-0
--  https://www.wowinterface.com/forums/showthread.php?t=54157
--      Easier profile/character/realm management
--      Specify defaults, so no uninitialized Db accesses
--      Blizzard's built-in "Defaults" button easier to use for config
function GHC.Functions.EnsureDbExists()
    -- TODO Consider having this return false if it is uninitialized
    --      ParseGuildBankData would have to have a special check then because it's okay being uninitialized
    if IsInGuild('player') and GetGuildInfo('player') then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if GhcDb == nil then
            GhcDb = {}
        end
        if GhcDb[guildName] == nil then
            GhcDb[guildName] = {
                GuildDb = {},
                GuildDbBackUp = {},
                GuildBank = {},
                GuildBankMoney = 0,
            }
        end
        return true
    end
    return false
end  

function GHC.Functions.ClearGuildRosterListView()
    for i = 1, 10 do
        GHC.GuildRosterListViewItems[i].RaceIcon.Texture:SetTexture(nil)
        GHC.GuildRosterListViewItems[i].ClassIcon.Texture:SetTexture(nil)
        GHC.GuildRosterListViewItems[i].Name:SetText('')
        GHC.GuildRosterListViewItems[i].Level:SetText('')
        GHC.GuildRosterListViewItems[i].RoleText:SetText('')
        GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(nil)
        GHC.GuildRosterListViewItems[i].Profession1 = ''
        GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(nil)
        GHC.GuildRosterListViewItems[i].Profession2 = ''
    end
end

function GHC.Functions.FilterByRole(role)
    if GHC.Functions.EnsureDbExists() then
        local filteredRoster = {}
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if GhcDb[guildName].GuildDb ~= nil then
            for k, member in pairs(GhcDb[guildName].GuildDb) do
                if member.Role == role then
                    tinsert(filteredRoster, member)
                end
            end
        end
        GHC.Functions.ClearGuildRosterListView()
        --reset listview scroll position
        GHC.UI.RosterScrollBar:SetValue(1)
        --update listview using first 10 members
        for i = 1, 10 do
            if filteredRoster[i] then
                if filteredRoster[i].Gender ~= nil and filteredRoster[i].Race ~= nil then
                    GHC.GuildRosterListViewItems[i].RaceIcon.Texture:SetTexture(GHC.Db.RaceIcons[filteredRoster[i].Gender][filteredRoster[i].Race])
                end
                GHC.GuildRosterListViewItems[i].ClassIcon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..filteredRoster[i].Class))
                if filteredRoster[i].MainsName == nil then
                    filteredRoster[i].MainsName = '-'
                end
                GHC.GuildRosterListViewItems[i].Name:SetText(tostring(filteredRoster[i].Name..' [|cffABD473'..filteredRoster[i].MainsName..'|r]'))
                GHC.GuildRosterListViewItems[i].Level:SetText(filteredRoster[i].Level)
                if filteredRoster[i].Role ~= nil then
                    GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[tonumber(filteredRoster[i].Role)])
                else
                    GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[0])
                end
                --profession 1
                if filteredRoster[i].Profession1Name ~= nil and filteredRoster[i].Profession1Level ~= nil then
                    GHC.GuildRosterListViewItems[i].Profession1Icon:Show()
                    GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[filteredRoster[i].Profession1Name])
                    GHC.GuildRosterListViewItems[i].Profession1 = tostring(filteredRoster[i].Profession1Name..' ['..filteredRoster[i].Profession1Level..']')
                else
                    GHC.GuildRosterListViewItems[i].Profession1Icon:Hide()
                    GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(nil)
                    GHC.GuildRosterListViewItems[i].Profession1 = ''
                end
                --profession 2
                if filteredRoster[i].Profession2Name ~= nil and filteredRoster[i].Profession2Level ~= nil then
                    GHC.GuildRosterListViewItems[i].Profession2Icon:Show()
                    GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[filteredRoster[i].Profession2Name])
                    GHC.GuildRosterListViewItems[i].Profession2 = tostring(filteredRoster[i].Profession2Name..' ['..filteredRoster[i].Profession2Level..']')
                else
                    GHC.GuildRosterListViewItems[i].Profession2Icon:Hide()
                    GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(nil)
                    GHC.GuildRosterListViewItems[i].Profession2 = ''
                end
            end
        end
    end
end

function GHC.Functions.FilterByProfession(prof)
    if GHC.Functions.EnsureDbExists() then
        local filteredRoster = {}
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if GhcDb[guildName].GuildDb ~= nil then
            for k, member in pairs(GhcDb[guildName].GuildDb) do
                if member.Profession1Name == prof or member.Profession2Name == prof then
                    tinsert(filteredRoster, member)
                end
            end
        end
        GHC.Functions.ClearGuildRosterListView()
        --reset listview scroll position
        GHC.UI.RosterScrollBar:SetValue(1)
        --update listview using first 10 members
        for i = 1, 10 do
            if filteredRoster[i] then
                if filteredRoster[i].Gender ~= nil and filteredRoster[i].Race ~= nil then
                    GHC.GuildRosterListViewItems[i].RaceIcon.Texture:SetTexture(GHC.Db.RaceIcons[filteredRoster[i].Gender][filteredRoster[i].Race])
                end
                GHC.GuildRosterListViewItems[i].ClassIcon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..filteredRoster[i].Class))
                if filteredRoster[i].MainsName == nil then
                    filteredRoster[i].MainsName = '-'
                end
                GHC.GuildRosterListViewItems[i].Name:SetText(tostring(filteredRoster[i].Name..' [|cffABD473'..filteredRoster[i].MainsName..'|r]'))
                GHC.GuildRosterListViewItems[i].Level:SetText(filteredRoster[i].Level)
                if filteredRoster[i].Role ~= nil then
                    GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[tonumber(filteredRoster[i].Role)])
                else
                    GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[0])
                end
                --profession 1
                if filteredRoster[i].Profession1Name ~= nil and filteredRoster[i].Profession1Level ~= nil then
                    GHC.GuildRosterListViewItems[i].Profession1Icon:Show()
                    GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[filteredRoster[i].Profession1Name])
                    GHC.GuildRosterListViewItems[i].Profession1 = tostring(filteredRoster[i].Profession1Name..' ['..filteredRoster[i].Profession1Level..']')
                else
                    GHC.GuildRosterListViewItems[i].Profession1Icon:Hide()
                    GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(nil)
                    GHC.GuildRosterListViewItems[i].Profession1 = ''
                end
                --profession 2
                if filteredRoster[i].Profession2Name ~= nil and filteredRoster[i].Profession2Level ~= nil then
                    GHC.GuildRosterListViewItems[i].Profession2Icon:Show()
                    GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[filteredRoster[i].Profession2Name])
                    GHC.GuildRosterListViewItems[i].Profession2 = tostring(filteredRoster[i].Profession2Name..' ['..filteredRoster[i].Profession2Level..']')
                else
                    GHC.GuildRosterListViewItems[i].Profession2Icon:Hide()
                    GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(nil)
                    GHC.GuildRosterListViewItems[i].Profession2 = ''
                end
            end
        end
    end
end


--scans the guild roster, creates a backup db which is checked against to update player info after roster scan
function GHC.Functions.ScanGuildRoster()
    --moved this inside an if check for player guild status, if no guild info available then do nothing
    if GHC.Functions.EnsureDbExists() then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')

        --create a back up of current guild db
        if GhcDb[guildName].GuildDb ~= nil then
            --wipe current backup - all info will be old or in current db
            GhcDb[guildName].GuildDbBackUp = {}
            for k, v in ipairs(GhcDb[guildName].GuildDb) do
                GhcDb[guildName].GuildDbBackUp[k] = v -- copy current db into new back up
            end
        end

        --wipe guild db
        GhcDb[guildName].GuildDb = {}
        local TotalMembers, OnlineMemebrs, _ = GetNumGuildMembers()
        local race, gender, prof1Name, prof1Level, prof2Name, prof2Level, role, mainsName = nil, nil, nil, nil, nil, nil, 0.0, '-'
        for i = 1, TotalMembers do
            local name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, class, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid = GetGuildRosterInfo(i)
            name = string.sub(name, 0, (string.find(name, '-') - 1))
            --if we have a back up check against it for existing data
            if GhcDb[guildName].GuildDbBackUp ~= nil then
                for k, member in pairs(GhcDb[guildName].GuildDbBackUp) do
                    if member.GUID == guid then
                        --print('found existing member')
                        race = member.Race
                        gender = member.Gender
                        prof1Name, prof1Level = member.Profession1Name, member.Profession1Level
                        prof2Name, prof2Level = member.Profession2Name, member.Profession2Level
                        role = tonumber(member.Role)
                        mainsName = member.MainsName
                    end
                end
            end
            GhcDb[guildName].GuildDb[i] = { GUID = guid, Name = name, Level = level, Class = class, Race = race, Gender = gender, Profession1Name = prof1Name, Profession1Level = prof1Level, Profession2Name = prof2Name, Profession2Level = prof2Level, Role = role, PublicNote = publicNote, MainsName = mainsName }
        end

        if GHC.Var.RosterSort == 'Name' or GHC.Var.RosterSort == 'Class' then
            table.sort(GhcDb[guildName].GuildDb, function(a, b) return a[GHC.Var.RosterSort] < b[GHC.Var.RosterSort] end)
        end
        if GHC.Var.RosterSort == 'Level' or GHC.Var.RosterSort == 'Role' then
            table.sort(GhcDb[guildName].GuildDb, function(a, b) return a[GHC.Var.RosterSort] > b[GHC.Var.RosterSort] end)
        end

        --clear current icons & text from listview
        GHC.Functions.ClearGuildRosterListView()

        --reset listview scroll position
        GHC.UI.RosterScrollBar:SetValue(1)
        --update listview using first 10 members - no sort function added yet
        for i = 1, 10 do
            GHC.GuildRosterListViewItems[i].ClassIcon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..GhcDb[guildName].GuildDb[i].Class))
            if GhcDb[guildName].GuildDb[i].MainsName == nil then
                GhcDb[guildName].GuildDb[i].MainsName = '-'
            end
            GHC.GuildRosterListViewItems[i].Name:SetText(tostring(GhcDb[guildName].GuildDb[i].Name..' [|cffABD473'..GhcDb[guildName].GuildDb[i].MainsName..'|r]'))
            GHC.GuildRosterListViewItems[i].Level:SetText(GhcDb[guildName].GuildDb[i].Level)
            if GhcDb[guildName].GuildDb[i].Role ~= nil then
                GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[tonumber(GhcDb[guildName].GuildDb[i].Role)])
            else
                GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[0])
            end
            --profession 1
            if GhcDb[guildName].GuildDb[i].Profession1Name ~= nil and GhcDb[guildName].GuildDb[i].Profession1Level ~= nil then
                GHC.GuildRosterListViewItems[i].Profession1Icon:Show()
                GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[GhcDb[guildName].GuildDb[i].Profession1Name])
                GHC.GuildRosterListViewItems[i].Profession1 = tostring(GhcDb[guildName].GuildDb[i].Profession1Name..' ['..GhcDb[guildName].GuildDb[i].Profession1Level..']')
            else
                GHC.GuildRosterListViewItems[i].Profession1Icon:Hide()
                GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(nil)
                GHC.GuildRosterListViewItems[i].Profession1 = ''
            end
            --profession 2
            if GhcDb[guildName].GuildDb[i].Profession2Name ~= nil and GhcDb[guildName].GuildDb[i].Profession2Level ~= nil then
                GHC.GuildRosterListViewItems[i].Profession2Icon:Show()
                GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[GhcDb[guildName].GuildDb[i].Profession2Name])
                GHC.GuildRosterListViewItems[i].Profession2 = tostring(GhcDb[guildName].GuildDb[i].Profession2Name..' ['..GhcDb[guildName].GuildDb[i].Profession2Level..']')
            else
                GHC.GuildRosterListViewItems[i].Profession2Icon:Hide()
                GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(nil)
                GHC.GuildRosterListViewItems[i].Profession2 = ''
            end
            if GhcDb[guildName].GuildDb[i].Gender ~= nil and GhcDb[guildName].GuildDb[i].Race ~= nil then
                GHC.GuildRosterListViewItems[i].RaceIcon.Texture:SetTexture(GHC.Db.RaceIcons[GhcDb[guildName].GuildDb[i].Gender][GhcDb[guildName].GuildDb[i].Race])
            end
        end

        --update the class summary, reset counts first then loop db and make new count > sort > update UI
        for k, class in pairs(GHC.Db.ClassCount) do
            class.Count = 0
        end
        local totalMembers = 0
        for i, member in ipairs(GhcDb[guildName].GuildDb) do
            for k, class in ipairs(GHC.Db.ClassCount) do
                if member.Class == class.Class then
                    class.Count = class.Count + 1
                    totalMembers = totalMembers + 1
                end
            end
        end
        table.sort(GHC.Db.ClassCount, function(a, b) return a.Count > b.Count end)
        for i, class in pairs(GHC.Db.ClassCount) do
            GHC.UI.ClassSummaryBars[i].StatusBar:SetValue((tonumber(class.Count) / tonumber(totalMembers)) * 100.0)
            GHC.UI.ClassSummaryBars[i].StatusBar:SetStatusBarColor(GHC.Db.ClassColours[class.Class].r, GHC.Db.ClassColours[class.Class].g, GHC.Db.ClassColours[class.Class].b)
            GHC.UI.ClassSummaryBars[i].Icon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..class.Class))
            GHC.UI.ClassSummaryBars[i].Text:SetText(tostring(class.Count.." ["..string.format("%.1f", (tonumber(class.Count) / tonumber(totalMembers)) * 100.0).."%]"))
        end
    end
end

function GHC.Functions.GuildRosterScrollBarChanged(value, rosterTable)
    if GHC.Functions.EnsureDbExists() then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        local v = math.ceil(value)
        GHC.UI.RosterScrollBar:SetMinMaxValues(1, math.ceil(#GhcDb[guildName].GuildDb/10))
        local i = 1
        GHC.Functions.ClearGuildRosterListView()

        if rosterTable == nil then rosterTable = GhcDb[guildName].GuildDb end

        --use i to access the listview item and k to access the guild db object
        for k, member in ipairs(rosterTable) do            
            if k < ((v * 10) + 1) and k > ((v - 1) * 10) then
                if rosterTable[k].Gender ~= nil and rosterTable[k].Race ~= nil then
                    GHC.GuildRosterListViewItems[i].RaceIcon.Texture:SetTexture(GHC.Db.RaceIcons[rosterTable[k].Gender][rosterTable[k].Race])
                else
                    GHC.GuildRosterListViewItems[i].RaceIcon.Texture:SetTexture(nil) --134400) --532) --607721)
                end
                --role
                if rosterTable[k].Role ~= nil then
                    GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[tonumber(rosterTable[k].Role)])
                else
                    GHC.GuildRosterListViewItems[i].RoleText:SetText(GHC.Db.RoleIconTextString[0])
                end
                GHC.GuildRosterListViewItems[i].ClassIcon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..rosterTable[k].Class))
                if rosterTable[k].MainsName == nil then
                    rosterTable[k].MainsName = '-'
                end
                GHC.GuildRosterListViewItems[i].Name:SetText(tostring(rosterTable[k].Name..' [|cffABD473'..rosterTable[k].MainsName..'|r]'))
                GHC.GuildRosterListViewItems[i].Level:SetText(rosterTable[k].Level)
                --profession 1
                if rosterTable[k].Profession1Name ~= nil and rosterTable[k].Profession1Level ~= nil then
                    GHC.GuildRosterListViewItems[i].Profession1Icon:Show()
                    GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[rosterTable[k].Profession1Name])
                    GHC.GuildRosterListViewItems[i].Profession1 = tostring(rosterTable[k].Profession1Name..' ['..rosterTable[k].Profession1Level..']')
                else
                    GHC.GuildRosterListViewItems[i].Profession1Icon:Hide()
                    GHC.GuildRosterListViewItems[i].Profession1Icon.Texture:SetTexture(nil)
                    GHC.GuildRosterListViewItems[i].Profession1 = ''
                end
                --profession 2
                if rosterTable[k].Profession2Name ~= nil and rosterTable[k].Profession2Level ~= nil then
                    GHC.GuildRosterListViewItems[i].Profession2Icon:Show()
                    GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[rosterTable[k].Profession2Name])
                    GHC.GuildRosterListViewItems[i].Profession2 = tostring(rosterTable[k].Profession2Name..' ['..rosterTable[k].Profession2Level..']')
                else
                    GHC.GuildRosterListViewItems[i].Profession2Icon:Hide()
                    GHC.GuildRosterListViewItems[i].Profession2Icon.Texture:SetTexture(nil)
                    GHC.GuildRosterListViewItems[i].Profession2 = ''
                end
                i = i + 1
            end
        end
    end
end

function GHC.Functions.SetMyData()
    if GHC.Functions.EnsureDbExists() then -- more can be moved in here
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if GhcDb[guildName].GuildDb ~= nil then
            for k, member in ipairs(GhcDb[guildName].GuildDb) do
                if member.GUID == UnitGUID('player') then
                    --print('set my data func')
                    member.MainsName = GHC.Var.MainsName
                    member.Level = UnitLevel('player')
                end
            end
        end
    end
end

function GHC.Functions.CreateMyDataString()
    local player = { Name = nil, Level = UnitLevel('player'), Class = nil, Race = nil, Gender = nil, Profession1Name = '-', Profession1Level = 0, Profession2Name = '-', Profession2Level = 0}

    _, class, _, race, gender, player.Name, _ = GetPlayerInfoByGUID(UnitGUID('player'))
    player.Race = string.upper(race)
    player.Class = string.upper(class)

    if gender == 3 then
        player.Gender = "FEMALE"
    else
        player.Gender = "MALE"
    end

    -- talent data isn't gathered at the moment and the intention is to avoid using it, instead players select their role(s) regardless of class/talents

    --will look at adding secondary prof data in a later release depending on community feedback, high level cooking is useful for food buffs and maybe useful to know
    for s = 1, GetNumSkillLines() do
        local skill, _, _, level, _, _, _, _, _, _, _, _, _ = GetSkillLineInfo(s)
        if skill == 'Fishing' then 
            --player.Fishing = level
        elseif skill == 'Cooking' then
            --player.Cooking = level
        elseif skill == 'First Aid' then
            --player.FirstAid = level
        else
            for k, prof in ipairs(GHC.Db.Professions) do
                if skill == prof.Name then
                    if player.Profession1Name == '-' then
                        player.Profession1Name = skill
                        player.Profession1Level = level
                    elseif player.Profession2Name == '-' then
                        player.Profession2Name = skill
                        player.Profession2Level = level
                    end
                end
            end
        end
    end
    local role = 0 --default as unknown roles
    if GhcDb[UnitGUID('player')].Role ~= nil then
        role = GhcDb[UnitGUID('player')].Role
    end
    mainsName = GHC.Var.MainsName
    if GHC.Functions.EnsureDbExists() then -- more can be moved in here
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if GhcDb[guildName].GuildDb ~= nil then
            for k, member in ipairs(GhcDb[guildName].GuildDb) do
                if member.GUID == UnitGUID('player') then
                    --print('grab mains name from db')
                    mainsName = member.MainsName
                end
            end
        end
    end
    --print(mainsName)
    dataString = strjoin(':', UnitGUID('player'), player.Name, player.Class, player.Race, player.Gender, player.Level, player.Profession1Name, player.Profession1Level, player.Profession2Name, player.Profession2Level, role, mainsName)
    return dataString
end

function GHC.Functions.SendMyData()
    addon:SendCommMessage("ghc-playerdata", GHC.Functions.CreateMyDataString(), "GUILD", nil, "BULK")
end

function GHC.Functions.ParsePlayerData(dataString)
    if GHC.Functions.EnsureDbExists() then
        local i = 1
        local dataSent = {}
        for d in string.gmatch(dataString, '[^:]+') do
            dataSent[i] = d
            i = i + 1
        end
        local addPlayer = true
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        GHC.Functions.EnsureDbExists()
        --this data is copied into the backup when a new roster scan occurs, its used to then update the new database after scan    
        if GhcDb[guildName].GuildDb ~= nil then
            for k, member in ipairs(GhcDb[guildName].GuildDb) do
                if member.GUID == dataSent[1] then
                    member.Name = dataSent[2]
                    member.Class = dataSent[3]
                    member.Race = dataSent[4]
                    member.Gender = dataSent[5]
                    member.Level = dataSent[6]
                    member.Profession1Name = dataSent[7]
                    member.Profession1Level = dataSent[8]
                    member.Profession2Name = dataSent[9]
                    member.Profession2Level = dataSent[10]
                    member.Role = dataSent[11]
                    member.MainsName = dataSent[12]
                    --print('updated', dataSent[2])
                    addPlayer = false
                end
            end
            --if player wasn't in db add new
            if addPlayer == true then
                tinsert(GhcDb[guildName].GuildDb, {GUID == dataSent[1], Name = dataSent[2], Class = dataSent[3], Race = dataSent[4], Gender = dataSent[5], Level = dataSent[6], Profession1Name = dataSent[7], Profession1Level = dataSent[8], Profession2Name = dataSent[9], Profession2Level = dataSent[10], Role = dataSent[11], MainsName = dataSent[12]} )
                --print('new player')
            end
        end
        GHC.Functions.ScanGuildRoster()
    end
end

function GHC.Functions.GuildBankStart(money)
    if GHC.Functions.EnsureDbExists() then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        GhcDb[guildName].GuildBank = {} -- wipe old bank data
        GhcDb[guildName].GuildBankMoney = money
        GHC.Var.ParseGuildBank = true
    end
end

function GHC.Functions.ScanGuildBank()
    if GHC.Functions.EnsureDbExists() then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if rankIndex == 0 or rankIndex == 1 then
            -- TODO Addon timestamp
            addon:Debug("ghc-gbdata-s", tostring(GetMoney()), "GUILD")
            addon:SendCommMessage("ghc-gbdata-s", tostring(GetMoney()), "GUILD")

            local ds = {}
            local c = 0

            -- scan basic bank slots
            for i = 1, 28 do
                local icon, itemCount, locked, quality, readable, lootable, itemLink, isFiltered, noValue, itemID = GetContainerItemInfo(-1, i)
                if itemLink then
                    tinsert(ds, {ID = itemID, Count = itemCount})
                    c = c + 1
                end
            end

            -- scan additional bags in bank slots
            for b = 5, 11 do
                for s = 1, GetContainerNumSlots(b) do
                    local icon, itemCount, locked, quality, readable, lootable, itemLink, isFiltered, noValue, itemID = GetContainerItemInfo(b, s)
                    if itemLink then
                        tinsert(ds, {ID = itemID, Count = itemCount})
                        c = c + 1
                    end
                end
            end

            -- scan character bags
            for bag = 0, 4 do
                for s = 1, GetContainerNumSlots(bag) do
                    local icon, itemCount, locked, quality, readable, lootable, itemLink, isFiltered, noValue, itemID = GetContainerItemInfo(bag, s)
                    if itemLink then
                        tinsert(ds, {ID = itemID, Count = itemCount})
                        c = c + 1
                    end
                end
            end

            local serialized = addon:Serialize(ds)
            addon:Debug("ghc-gbdata", serialized, "GUILD")
            addon:SendCommMessage("ghc-gbdata", serialized, "GUILD")

            addon:Debug("ghc-gbdata-f", "blank", "GUILD")
            addon:SendCommMessage("ghc-gbdata-f", "blank", "GUILD")
        end
    end
end

function GHC.Functions.SendGuildBankData()
    if GHC.Functions.EnsureDbExists() then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        -- TODO Addon timestamp
        addon:Debug("ghc-gbdata-s", tostring(GhcDb[guildName].GuildBankMoney), "GUILD")
        addon:SendCommMessage("ghc-gbdata-s", tostring(GhcDb[guildName].GuildBankMoney), "GUILD")

        -- remove these after functionality confirmed
        local gbDbSize = #GhcDb[guildName].GuildBank
        local packets = math.floor(gbDbSize/20)
        local remain = (gbDbSize - (packets * 20))
        local lastPos = gbDbSize - remain

        -- Only send necessary pieces of GuildBank to minimize message size
        local ds = {}
        for i, item in ipairs(GhcDb[guildName].GuildBank) do
            tinsert(ds, {ID = item.ID, Count = item.Count})
        end

        local serialized = addon:Serialize(ds)
        addon:Debug("ghc-gbdata", serialized, "GUILD")
        addon:SendCommMessage("ghc-gbdata", serialized, "GUILD")

        addon:Debug("ghc-gbdata-f", "blank", "GUILD")
        addon:SendCommMessage("ghc-gbdata-f", "blank", "GUILD")
    end
end

-- close guild bank parsing
function GHC.Functions.GuildBankFinish()
    GHC.Var.ParseGuildBank = false
end

function GHC.Functions.ParseGuildBankData(dataString)
    if dataString and GHC.Var.ParseGuildBank == true then
        if GHC.Functions.EnsureDbExists() then
            local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
            local success, ds = addon:Deserialize(dataString)
            if success then
                -- TODO Deleting existing GuildBank is already done GuildBankStart
                -- It might make more sense to do it here only in case you only receive the start
                -- but never the subsequent data, so you'd like to keep your old data at least
                -- Wipe out existing GuildBank and replace with newly received one
                GhcDb[guildName].GuildBank = {}
                for i, item in ipairs(ds) do
                    item.Type = select(6, GetItemInfo(item.ID))
                    addon:Debug(i, item.ID, item.Count, item.Type)
                    tinsert(GhcDb[guildName].GuildBank, item)
                end
                GHC.Functions.UpdateGuildBankGridView(1)
            else
                addon:Print("error deserializing:", ds)
                addon:Print("Make sure", GHC.addon_name, "is up to date!")
            end
        end
    end
end

function GHC.Functions.UpdateGuildBankGridView(value)
    if GHC.Functions.EnsureDbExists() then
        local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
        if GhcDb[guildName].GuildBankMoney ~= nil then
            GHC.UI.GuildBankParentFrame.MoneyText:SetText(GetCoinTextureString(GhcDb[guildName].GuildBankMoney))
        end
        if GhcDb[guildName].GuildBank ~= nil then
            --table.sort(GhcDb[guildName].GuildBank, function(a, b) return a.Type < b.Type end)
            local v = math.ceil(value)
            for i = 1, 96 do
                GHC.UI.GuildBankGridViewItems[i].Texture:SetTexture(nil)
                GHC.UI.GuildBankGridViewItems[i].Text:SetText(nil)
                GHC.UI.GuildBankGridViewItems[i].Item = nil
                GHC.UI.GuildBankGridViewItems[i].Link = nil
            end
            for k, item in pairs(GhcDb[guildName].GuildBank) do
                if k < ((v * 88) + 1) and k > ((v - 1) * 88) then
                    GHC.UI.GuildBankGridViewItems[k].Texture:SetTexture(GetItemIcon(item.ID))
                    GHC.UI.GuildBankGridViewItems[k].Text:SetText(item.Count)
                    GHC.UI.GuildBankGridViewItems[k].Item = GetItemInfo(item.ID)
                    GHC.UI.GuildBankGridViewItems[k].Link = select(2, GetItemInfo(item.ID))
                end
            end
        end
    end
end

function GHC.Functions.ShowGuildBankTooltip(frame, text)
    GameTooltip:SetOwner(frame) --, "ANCHOR_CURSOR")
    GameTooltip:SetPoint("TOPLEFT")
    if frame.Link then
        GameTooltip:SetHyperlink(frame.Link)
        GameTooltip:Show()
    end
end


function GHC_SummaryFrameOnShow()
    
end

function GHC_BankFrameOnShow()
    C_Timer.After(1.5, function() GHC.Functions.UpdateGuildBankGridView(1) end)
end

function GHC_RosterFrameOnShow()
    C_Timer.After(1.5, function() GHC.Functions.ScanGuildRoster() end)
end
--print('loaded scripts')


-- CORE
function addon:PlayerDataCommHandler(prefix, message, distribution, sender)
    self:Debug("PlayerDataCommHandler", prefix, strlen(message), message, distribution, sender)
    GHC.Functions.ParsePlayerData(message)
end


function addon:GuildBankMoneyCommHandler(prefix, message, distribution, sender)
    self:Debug("GuildBankMoneyCommHandler", prefix, strlen(message), message, distribution, sender)
    GHC.UI.GuildBankParentFrame.MoneyText:SetText(GetCoinTextureString(tostring(message)))
    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named since
    -- AceComm handles packetizing large messages automatically
    GHC.Functions.GuildBankStart(message)
end


function addon:GuildBankDataCommHandler(prefix, message, distribution, sender)
    self:Debug("GuildBankDataCommHandler", prefix, strlen(message), message, distribution, sender)
    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named since
    -- AceComm handles packetizing large messages automatically
    GHC.Functions.ParseGuildBankData(message)
end


function addon:GuildBankFinishCommHandler(prefix, message, distribution, sender)
    self:Debug("GuildBankFinishCommHandler", prefix, strlen(message), message, distribution, sender)
    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named
    GHC.Functions.GuildBankFinish()
end


function addon:UpdateMeCommHandler(prefix, message, distribution, sender)
    self:Debug("UpdateMeCommHandler", prefix, strlen(message), message, distribution, sender)
    if GetAddOnMetadata("GuildHelperClassic", "Version") ~= message then
        self:Print("New version available!")
        GhcDb[UnitGUID("player")].NewVersion = message
    end
end


-- Default Comm handler
function addon:OnCommReceived(prefix, message, distribution, sender)
    self:Debug("OnCommReceived", prefix, strlen(message), message, distribution, sender)
end


function addon:GUILD_ROSTER_UPDATE(eventName)
    self:Debug(eventName)
    -- TODO Is this required now that the ui is self contained?
    local t = C_Timer.After(3, function() GHC.Functions.ScanGuildRoster() end)
end


function addon:PlayerDataChangedHandler(eventName)
    self:Debug(eventName)
    if GhcDb[UnitGUID('player')].AutoSendCharacterData == true then
        local t = C_Timer.After(2, function() GHC.Functions.SendMyData() end)
    end    
end


function addon:ChatCommand(msg)
    if msg == nil or msg == "" then
        msg = "log"
    end
    commands = {
        -- ["help"] = function() self:Print("Available commands are: open, scangb, fix, help") end,
        ["open"] = function() GHC.UI.Toggle() end,
        ["fix"] = function() StaticPopup_Show("GHC_FixMe") end,
        ["scangb"] = GHC.Functions.ScanGuildBank,
        ["debug"] = function() DEBUG = not DEBUG; self:Print("DEBUG =", DEBUG) end
    }
    local prefix, nextposition = self:GetArgs(msg, 1)
    local cmd = commands[prefix]
    if cmd ~= nil then
        cmd()
    else
        local message = self:GetArgs(msg, 1, nextposition)
        if prefix == nil or message == nil then
            self:Print("Available commands are: open, scangb, fix, help")
        else
            self:Print("GHC_Slash: cmd:", prefix, message)
            self:SendCommMessage(prefix, message, "GUILD")
        end
    end
end


function addon:Debug(...)
    if DEBUG then
        self:Print(...)
    end
end


-- Code that you want to run when the addon is first loaded goes here.
function addon:OnInitialize()
    self:RegisterChatCommand("ghc", "ChatCommand")

    if not GhcLDBIconDb then
        -- local ldbDb = LibStub("AceDB-3.0"):New("GhcLDBIconDb", { profile = { minimap = { hide = false, }, }, })
        GhcLDBIconDb = { hide = false, }
    end
    GHC.UI.MinimapIcon:Register(GHC.addon_name, GHC.UI.MinimapButton, GhcLDBIconDb)
end


-- The OnEnable() and OnDisable() methods of your addon object are called by AceAddon when
-- your addon is enabled/disabled by the user.
-- Unlike OnInitialize(), this may occur multiple times without the entire UI being reloaded.

function addon:OnEnable()
    -- restore saved settings
    -- register events
    -- register library callbacks
    -- check Saved Variables for frames' OnShow
    -- or other "on" setup
    self:RegisterComm("ghc-update-me", "UpdateMeCommHandler")
    self:RegisterComm("ghc-playerdata", "PlayerDataCommHandler")
    self:RegisterComm("ghc-gbdata-s", "GuildBankMoneyCommHandler")
    self:RegisterComm("ghc-gbdata", "GuildBankDataCommHandler")
    self:RegisterComm("ghc-gbdata-f", "GuildBankFinishCommHandler")

    local t = C_Timer.After(1, function()
        GHC.UI.DrawGuildRosterListview() 
        GHC.UI.DrawGuildClassSummaryBars() 
        GHC.UI.DrawGuildBankGridView()
        GHC.Functions.Load() 
        GHC.UI.LoadNewUI() 
        -- GHC.UI.PlayerOptionsFrame.PlayerRoleDropdown_Init()
        GHC.UI.RosterScrollFrame.FilterProfDropDown_Init()
        GHC.UI.RosterScrollFrame.FilterRoleDropDown_Init()
    end)

    self:RegisterEvent("GUILD_ROSTER_UPDATE")
    self:RegisterEvent("CHAT_MSG_SKILL", "PlayerDataChangedHandler")
    self:RegisterEvent("PLAYER_LEVEL_UP", "PlayerDataChangedHandler")
end


function addon:OnDisable()
    -- AceEvent-3.0 automatically handles unregistering events
end
