--[==[

Copyright ©2019 Samuel Thomas Pain

The contents of this addon, excluding third-party resources, are
copyrighted to their authors with all rights reserved.

This addon is free to use and the authors hereby grants you the following rights:

1. 	You may make modifications to this addon for private use only, you
    may not publicize any portion of this addon.

2. 	Do not modify the name of this addon, including the addon folders.

3. 	This copyright notice shall be included in all copies or substantial
    portions of the Software.

All rights not explicitly addressed in this license are reserved by
the copyright holders.

]==]--

local name, GHC = ...

GHC.addon_name = "Guild Helper Classic"

GHC.Db = {}

GHC.Db.Roles = {
    { Id = 1, Text = 'DPS' },
    { Id = 2, Text = 'Healer' },
    { Id = 3, Text = 'Tank' },
    { Id = 4, Text = 'DPS-Healer' },
    { Id = 5, Text = 'DPS-Tank' },
    { Id = 6, Text = 'Healer-Tank' },
    { Id = 7, Text = 'Any' },
    { Id = 8, Text = 'Guild Bank' },
}

GHC.Db.RoleIconTextString = {
    [0] = 'Unknown Role',
    [1] = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t", --dps
    [2] = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:1:20|t", --heal
    [3] = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:0:19:22:41|t", --tank
    [4] = tostring("|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t".." |TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:1:20|t"), --pds/heal
    [5] = tostring("|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t".."|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:0:19:22:41|t"), --dps/tank
    [6] = tostring("|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:1:20|t".."|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:0:19:22:41|t"), --heal/tank
    [7] = tostring("|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t".."|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:1:20|t".."|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:0:19:22:41|t"), --all
    [8] = 'Guild Bank',
}

--need to confirm the undead are known as scourge?
GHC.Db.RaceIcons = {
    FEMALE = {
        HUMAN = 130904,
        DWARF = 130902,
        NIGHTELF = 130905,
        GNOME = 130903,
        ORC = 130906,
        TROLL = 130909,
        TAUREN = 130908,
        SCOURGE = 130907
    },
    MALE = {
        HUMAN = 130914,
        DWARF = 130912,
        NIGHTELF = 130915,
        GNOME = 130913,
        ORC = 130916,
        TROLL = 130919,
        TAUREN = 130918,
        SCOURGE = 130917
    }
}

GHC.Db.RaceIconsAlt = {
    FEMALE = {
        HUMAN = 236447,
        DWARF = 236443,
        NIGHTELF = 236449,
        GNOME = 236445,
        ORC = 236451,
        TROLL = 236455,
        TAUREN = 236453,
        SCOURGE = 236457
    },
    MALE = {
        HUMAN = 236448,
        DWARF = 236444,
        NIGHTELF = 236450,
        GNOME = 236446,
        ORC = 236452,
        TROLL = 236456,
        TAUREN = 236454,
        SCOURGE = 236458
    }
}

--update these to use file IDs
GHC.Db.ProfessionIcons = {
	Alchemy = 'Interface\\Icons\\Trade_Alchemy' ,
	Blacksmithing = 'Interface\\Icons\\Trade_Blacksmithing' ,
	Enchanting = 'Interface\\Icons\\Trade_Engraving' ,
	Engineering = 'Interface\\Icons\\Trade_Engineering' ,
	Inscription = 'Interface\\Icons\\INV_Inscription_Tradeskill01' ,
	Jewelcrafting = 'Interface\\Icons\\INV_MISC_GEM_01' ,
	Leatherworking = 'Interface\\Icons\\INV_Misc_ArmorKit_17' ,
	Tailoring = 'Interface\\Icons\\Trade_Tailoring' ,
	Herbalism = 'Interface\\Icons\\INV_Misc_Flower_02' ,
	Skinning = 'Interface\\Icons\\INV_Misc_Pelt_Wolf_01' ,
	Mining = 'Interface\\Icons\\Spell_Fire_FlameBlades' ,
	Cooking = 135805, --'Interface\\Icons\\Trade_Cooking_2' ,
	Fishing = 136245, --'Interface\\Icons\\Trade_Fishing' ,
	FirstAid = 'Interface\\Icons\\Spell_Holy_SealOfSacrifice'
}

--potential feature maybe ? NOT using talents so to be removed?
GHC.Db.SpecIcons = {
	DRUID = { Balance = 'Interface\\Icons\\Spell_Nature_Starfall', Feral = 132276, Restoration = 'Interface\\Icons\\Spell_Nature_HealingTouch' },
	DEATHKNIGHT = { Frost = '', Blood = '', Unholy = ''},
	HUNTER = { BeastMastery = 132164, Marksmanship = 132222, Survival = 132215},
	ROGUE = { Assassination = 132292, Combat = 132090, Subtlety = 132089},
	MAGE = { Frost = '', Fire = '', Arcane = ''},
	PRIEST = { Holy = '', Discipline = '', Shadow = ''},
	SHAMAN = { Elemental = 'Interface\\Icons\\Spell_Nature_Lightning', Enhancement = 'Interface\\Icons\\Spell_Nature_LightningShield', Restoration = 'Interface\\Icons\\Spell_Nature_MagicImmunity' },
	WARLOCK = { Demonology = '', Affliction = 'Interface\\Icons\\Spell_Shadow_DeathCoil', Destruction = ''},
	WARRIOR = { Arms = 132292, Fury = 132347, Protection = 132341},
	PALADIN = { Retribution = 135873, Holy = 135920, Protection = 135893},
}

GHC.Db.Professions = {
	{ Id = 1, Name = 'Alchemy' },
	{ Id = 2, Name = 'Blacksmithing' },
	{ Id = 3, Name = 'Enchanting' },
	{ Id = 4, Name = 'Engineering' },
	--{ Id = 5, Name = 'Inscription' },
	--{ Id = 6, Name = 'Jewelcrafting' },
	{ Id = 7, Name = 'Leatherworking' },
	{ Id = 8, Name = 'Tailoring' },
	{ Id = 9, Name = 'Herbalism' },
	{ Id = 10, Name = 'Skinning' },
	{ Id = 11, Name = 'Mining' },
}

GHC.Db.ClassColours = {
	DEATHKNIGHT = { r = 0.77, g = 0.12, b = 0.23, fs = '|cffC41F3B' }, -- ready for wrath :)
	DRUID = { r = 1.00, g = 0.49, b = 0.04, fs = '|cffFF7D0A' },
	HUNTER = { r = 0.67, g = 0.83, b = 0.45, fs = '|cffABD473' },
	MAGE = { r = 0.25, g = 0.78, b = 0.92, fs = '|cff40C7EB' },
	PALADIN = { r = 0.96, g = 0.55, b = 0.73, fs = '|cffF58CBA' },
	PRIEST = { r = 1.00, g = 1.00, b = 1.00, fs = '|cffFFFFFF' },
	ROGUE = { r = 1.00, g = 0.96, b = 0.41, fs = '|cffFFF569' },
	SHAMAN = { r = 0.00, g = 0.44, b = 0.87, fs = '|cff0070DE' },
	WARLOCK = { r = 0.53, g = 0.53, b =	0.93, fs = '|cff8787ED' },
	WARRIOR = { r = 0.78, g = 0.61, b = 0.43, fs = '|cffC79C6E' },
	Total = { r = 1, g = 1, b = 1 }
}

GHC.Db.ClassIDs = { 'WARRIOR', 'DRUID', 'PALADIN', 'PRIEST', 'SHAMAN', 'HUNTER', 'MAGE', 'ROGUE', 'WARLOCK' }

GHC.Db.ClassCount = {
	--{ Class = 'DEATHKNIGHT', Count = 0 },
	{ Class = 'DRUID', Count = 0 },
	{ Class = 'HUNTER', Count = 0 },
	{ Class = 'MAGE', Count = 0 },
	{ Class = 'PALADIN', Count = 0 },
	{ Class = 'PRIEST', Count = 0 },
	{ Class = 'ROGUE', Count = 0 },
	{ Class = 'SHAMAN', Count = 0 },
	{ Class = 'WARLOCK', Count = 0},
	{ Class = 'WARRIOR', Count  = 0 }
}