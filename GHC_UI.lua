--[==[

Copyright ©2019 Samuel Thomas Pain

The contents of this addon, excluding third-party resources, are
copyrighted to their authors with all rights reserved.

This addon is free to use and the authors hereby grants you the following rights:

1. 	You may make modifications to this addon for private use only, you
    may not publicize any portion of this addon.

2. 	Do not modify the name of this addon, including the addon folders.

3. 	This copyright notice shall be included in all copies or substantial
    portions of the Software.

All rights not explicitly addressed in this license are reserved by
the copyright holders.

]==]--


local name, GHC = ...


GHC.UI = { 
    GuildRosterListViewItemFontSizeLarge = 14.0, 
    GuildRosterListViewItemFontSizeSmall = 11.0,
    GuildRosterProfessionIconSize = 17.0,
    ClassSummaryBarHeight = 25.0,
    ClassSummaryBarWidth = 150.0,
    GuildRosterListViewItemFontTooltip = 11.0,
    GuildBankGridviewIconSize = 36,
    GuildBankGridviewIconTextFontSize = 10
}

--this is now just an event frame
GHC.UI.ParentFrame = CreateFrame("FRAME", "GHC_ParentFrame", UIParent)
--[==[
GHC.UI.ParentFrame:SetPoint("TOPLEFT", 350, -12)
GHC.UI.ParentFrame:SetBackdrop({ edgeFile = "interface/dialogframe/ui-dialogbox-border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
GHC.UI.ParentFrame:SetSize(975, 500)
GHC.UI.ParentFrame.Texture = GHC.UI.ParentFrame:CreateTexture('$parent_Texture', 'BACKGROUND')
GHC.UI.ParentFrame.Texture:SetTexture("interface/dialogframe/ui-dialogbox-background")
GHC.UI.ParentFrame.Texture:SetPoint('TOPLEFT', 4, -4)
GHC.UI.ParentFrame.Texture:SetPoint('BOTTOMRIGHT', -4, 4)
GHC.UI.ParentFrame:Hide()
]==]--

GHC.UI.RosterScrollFrame = CreateFrame("SCROLLFRAME", "GHC_GuildRosterScrollframe", GuildHelperClassic_RosterFrame)
GHC.UI.RosterScrollFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.RosterScrollFrame:SetSize(400, 405)

GHC.UI.RosterScrollFrame.SortNameButton = CreateFrame("BUTTON", "GHC_SortNameButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortNameButton:SetPoint("TOPLEFT", 64, 22)
GHC.UI.RosterScrollFrame.SortNameButton:SetText('Name')
GHC.UI.RosterScrollFrame.SortNameButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortNameButton:SetScript("OnClick", function() GHC.Var.RosterSort = 'Name' GHC.Functions.ScanGuildRoster() end)

--[==[
GHC.UI.RosterScrollFrame.SortRoleButton = CreateFrame("BUTTON", "GHC_SortRoleButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortRoleButton:SetPoint("TOPLEFT", 114, 22)
GHC.UI.RosterScrollFrame.SortRoleButton:SetText('Role')
GHC.UI.RosterScrollFrame.SortRoleButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortRoleButton:SetScript("OnClick", function() GHC.Var.RosterSort = 'Role' GHC.Functions.ScanGuildRoster() end)
]==]--

GHC.UI.RosterScrollFrame.SortLevelButton = CreateFrame("BUTTON", "GHC_SortLevelButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortLevelButton:SetPoint("TOPLEFT", 290, 22)
GHC.UI.RosterScrollFrame.SortLevelButton:SetText('Level')
GHC.UI.RosterScrollFrame.SortLevelButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortLevelButton:SetScript("OnClick", function() GHC.Var.RosterSort = 'Level' GHC.Functions.ScanGuildRoster() end)

GHC.UI.RosterScrollFrame.SortClassButton = CreateFrame("BUTTON", "GHC_SortClassButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortClassButton:SetPoint("TOPLEFT", 340, 22)
GHC.UI.RosterScrollFrame.SortClassButton:SetText('Class')
GHC.UI.RosterScrollFrame.SortClassButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortClassButton:SetScript("OnClick", function() GHC.Var.RosterSort = 'Class' GHC.Functions.ScanGuildRoster() end)

GHC.UI.RosterScrollFrame.FilterRoleDropDown = CreateFrame("FRAME", "GHC_PlayerRoleDropdown", GHC.UI.RosterScrollFrame, "UIDropDownMenuTemplate")
GHC.UI.RosterScrollFrame.FilterRoleDropDown:SetPoint("TOPLEFT", 100, 26)
GHC.UI.RosterScrollFrame.FilterRoleDropDown.displayMode = nil --"MENU"
UIDropDownMenu_SetWidth(GHC.UI.RosterScrollFrame.FilterRoleDropDown, 50)
UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterRoleDropDown, 'Role')
function GHC.UI.RosterScrollFrame.FilterRoleDropDown_Init()
	UIDropDownMenu_Initialize(GHC.UI.RosterScrollFrame.FilterRoleDropDown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
		if (level or 1) == 1 then
			for k, role in ipairs(GHC.Db.Roles) do
                info.text = role.Text
                info.arg1 = role.Id
                info.arg2 = nil						
                info.func = function() GHC.Functions.FilterByRole(role.Id) UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterRoleDropDown, role.Text) end
                info.isNotRadio = true
                --info.hasArrow = true
                --info.menuList = class.MenuList
                UIDropDownMenu_AddButton(info)
			end
		end
	end)
end

GHC.UI.RosterScrollFrame.FilterProfDropDown = CreateFrame("FRAME", "GHC_PlayerRoleDropdown", GHC.UI.RosterScrollFrame, "UIDropDownMenuTemplate")
GHC.UI.RosterScrollFrame.FilterProfDropDown:SetPoint("TOPLEFT", 170, 26)
GHC.UI.RosterScrollFrame.FilterProfDropDown.displayMode = nil --"MENU"
UIDropDownMenu_SetWidth(GHC.UI.RosterScrollFrame.FilterProfDropDown, 80)
UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterProfDropDown, 'Profession')
function GHC.UI.RosterScrollFrame.FilterProfDropDown_Init()
	UIDropDownMenu_Initialize(GHC.UI.RosterScrollFrame.FilterProfDropDown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
		if (level or 1) == 1 then
			for k, prof in ipairs(GHC.Db.Professions) do
                info.text = prof.Name
                info.arg1 = prof.Id
                info.arg2 = nil						
                info.func = function() GHC.Functions.FilterByProfession(prof.Name) UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterProfDropDown, prof.Name) end
                info.isNotRadio = true
                --info.hasArrow = true
                --info.menuList = class.MenuList
                UIDropDownMenu_AddButton(info)
			end
		end
	end)
end

--[==[
GHC.UI.RosterScrollFrame.Header = GHC.UI.RosterScrollFrame:CreateFontString("GHC_PlayerOptionsFrame_Header", 'OVERLAY', 'GameFontNormal')
GHC.UI.RosterScrollFrame.Header:SetPoint("TOP", 0, 16)
GHC.UI.RosterScrollFrame.Header:SetText("Members")
GHC.UI.RosterScrollFrame.Header:SetTextColor(1,1,1,1)
GHC.UI.RosterScrollFrame.Header:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))
]==]--

GHC.UI.RosterScrollBar = CreateFrame("Slider", "AJ_ConsumablesListViewFrame_ScrollFrame", GHC.UI.RosterScrollFrame, "UIPanelScrollBarTemplate")
GHC.UI.RosterScrollBar:SetPoint("TOPLEFT", GHC.UI.RosterScrollFrame, "TOPRIGHT", -16, -22) 
GHC.UI.RosterScrollBar:SetPoint("BOTTOMLEFT", GHC.UI.RosterScrollFrame, "BOTTOMRIGHT", -16, 8)
GHC.UI.RosterScrollBar:SetMinMaxValues(1, 10)
GHC.UI.RosterScrollBar:SetValueStep(1.0)
GHC.UI.RosterScrollBar.scrollStep = 1
GHC.UI.RosterScrollBar:SetValue(1)
GHC.UI.RosterScrollBar:SetWidth(16)
GHC.UI.RosterScrollBar:SetScript('OnValueChanged', function(self, value) GHC.Functions.GuildRosterScrollBarChanged(value) end)

GHC.GuildRosterListViewItems = {}
function GHC.UI.DrawGuildRosterListview()
    for i = 1, 10 do
        local f = CreateFrame("FRAME", tostring("GHC_GuildRosterListViewItem"..i), GHC.UI.RosterScrollFrame)
        f:SetPoint("TOPLEFT", 12, ((41 * (i - 1)) * -1) - 2)
        f:SetSize(370, 41)
        f:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", insets = { left = 1, right = 1, top = 1, bottom = 1 } })
        f:SetBackdropColor(0, 0, 0, 1.0)

        f.RaceIcon = CreateFrame("FRAME", tostring("GHC_GuildRosterListViewItem"..i.."_RaceIcon"), f)
        f.RaceIcon:SetPoint("LEFT", 6, 0)
        f.RaceIcon:SetSize(32, 32)
        f.RaceIcon.Texture = f.RaceIcon:CreateTexture("$parent_Background", "BACKGROUND")
        f.RaceIcon.Texture:SetAllPoints(f.RaceIcon)

        f.RoleText = f:CreateFontString(tostring("GHC_GuildRosterListViewItem"..i.."_RoleText"), "OVERLAY", "GameFontNormal")
        f.RoleText:SetPoint("TOPLEFT", 54, -24)
        
        f.ClassIcon = CreateFrame("FRAME", tostring("GHC_GuildRosterListViewItem"..i.."_RaceIcon"), f)
        f.ClassIcon:SetPoint("RIGHT", -2, -2)
        f.ClassIcon:SetSize(40, 40)
        f.ClassIcon.Texture = f.ClassIcon:CreateTexture("$parent_Background", "BACKGROUND")
        f.ClassIcon.Texture:SetAllPoints(f.ClassIcon)

        f.Name = f:CreateFontString(tostring("GHC_GuildRosterListViewItem"..i.."_NameText"), 'OVERLAY', 'GameFontNormal')
        f.Name:SetPoint("TOPLEFT", 52, -3)
        f.Name:SetTextColor(1,1,1,1)
        f.Name:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))

        f.Level = f:CreateFontString(tostring("GHC_GuildRosterListViewItem"..i.."_LevelText"), 'OVERLAY', 'GameFontNormal')
        f.Level:SetPoint("RIGHT", -52, 0)
        f.Level:SetTextColor(1,1,1,1)
        f.Level:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge + 3))

        f.Profession1Icon = CreateFrame("FRAME", tostring("GHC_GuildMemberDetailFrame_Profession1Icon"), f)
        f.Profession1Icon:SetPoint("BOTTOMRIGHT", -90, 3)
        f.Profession1Icon:SetSize(tonumber(GHC.UI.GuildRosterProfessionIconSize), tonumber(GHC.UI.GuildRosterProfessionIconSize))
        f.Profession1Icon.Texture = f.Profession1Icon:CreateTexture("$parent_Background", "BACKGROUND")
        f.Profession1Icon.Texture:SetAllPoints(f.Profession1Icon)
        f.Profession1 = nil
        f.Profession1Icon:SetScript("OnEnter", function() GHC.Functions.ShowTooltip(f.Profession1Icon, f.Profession1) end)
        f.Profession1Icon:SetScript("OnLeave", function() GHC.UI.Tooltip:Hide() end)

        f.Profession2Icon = CreateFrame("FRAME", tostring("GHC_GuildMemberDetailFrame_Profession2Icon"), f)
        f.Profession2Icon:SetPoint("BOTTOMRIGHT", -120, 3)
        f.Profession2Icon:SetSize(tonumber(GHC.UI.GuildRosterProfessionIconSize), tonumber(GHC.UI.GuildRosterProfessionIconSize))
        f.Profession2Icon.Texture = f.Profession2Icon:CreateTexture("$parent_Background", "BACKGROUND")
        f.Profession2Icon.Texture:SetAllPoints(f.Profession2Icon)
        f.Profession2 = nil
        f.Profession2Icon:SetScript("OnEnter", function() GHC.Functions.ShowTooltip(f.Profession2Icon, f.Profession2) end)
        f.Profession2Icon:SetScript("OnLeave", function() GHC.UI.Tooltip:Hide() end)
        
        f.PublicNote = nil
        --f.RaceIcon:SetScript("OnEnter", function(self) GHC.Functions.ShowTooltip(f.RaceIcon, f.PublicNote) end)
        --f.RaceIcon:SetScript("OnLeave", function() GHC.UI.Tooltip:Hide() end)

        GHC.GuildRosterListViewItems[i] = f
    end
end

GHC.UI.GuildSummaryFrame = CreateFrame("FRAME", "GHC_GuildSummaryFrame", GuildHelperClassic_SummaryFrame)
--GHC.UI.GuildSummaryFrame:SetBackdrop({ edgeFile = "Interface/Tooltips/UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
GHC.UI.GuildSummaryFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.GuildSummaryFrame:SetSize(400, 405)

GHC.UI.GuildSummaryFrameTitle = GHC.UI.GuildSummaryFrame:CreateFontString("GHC_GuildSummaryTitle", 'OVERLAY', 'GameFontNormal')
GHC.UI.GuildSummaryFrameTitle:SetPoint("TOP", 0, -8)
GHC.UI.GuildSummaryFrameTitle:SetText("Class Summary")
GHC.UI.GuildSummaryFrameTitle:SetTextColor(1,1,1,1)
GHC.UI.GuildSummaryFrameTitle:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))

GHC.UI.ClassSummaryBars = {}
function GHC.UI.DrawGuildClassSummaryBars()
    for k, v in ipairs(GHC.Db.ClassIDs) do
        local f = CreateFrame("FRAME", tostring("GHC_GuildSummaryClassFrame"..k), GHC.UI.GuildSummaryFrame)
        f:SetSize(260, 44)
        f:SetPoint("TOPLEFT", 8, (k * GHC.UI.ClassSummaryBarHeight) * -1)

        f.Icon = CreateFrame("FRAME", tostring("GHC_GuildSummaryClassFrame_Icon"..k), f)
        f.Icon:SetPoint("LEFT", 4, 0)
        f.Icon:SetSize(GHC.UI.ClassSummaryBarHeight, GHC.UI.ClassSummaryBarHeight)
        f.Icon.Texture = f.Icon:CreateTexture("$parent_Background", "BACKGROUND")
        f.Icon.Texture:SetAllPoints(f.Icon)
        f.Icon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..v))

        f.Text = f:CreateFontString(tostring("GHC_GuildSummaryClassFrame_Name"..k), 'OVERLAY', 'GameFontNormal')
        f.Text:SetPoint("RIGHT", 0, 0)
        f.Text:SetTextColor(1,1,1,1)
        f.Text:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeSmall))
        f.Text:SetText(v)  
        
        f.StatusBar = CreateFrame("StatusBar", tostring("GHC_GuildSummaryClassFrame_StatusBar"..k), f)
        f.StatusBar:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
        f.StatusBar:GetStatusBarTexture():SetHorizTile(false)
        f.StatusBar:SetMinMaxValues(0, 100)
        f.StatusBar:SetSize(GHC.UI.ClassSummaryBarWidth, GHC.UI.ClassSummaryBarHeight * 0.9)
        f.StatusBar:SetPoint('LEFT', 40, 0)
        f.StatusBar:SetStatusBarColor(GHC.Db.ClassColours[v].r, GHC.Db.ClassColours[v].g, GHC.Db.ClassColours[v].b)	

        GHC.UI.ClassSummaryBars[k] = f
    end
end


GHC.UI.PlayerOptionsFrame = CreateFrame("FRAME", "GHC_PlayerOptionsFrame", GuildHelperClassic_OptionsFrame)
--GHC.UI.PlayerOptionsFrame:SetBackdrop({ edgeFile = "Interface/Tooltips/UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
GHC.UI.PlayerOptionsFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.PlayerOptionsFrame:SetSize(400, 405)

GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton = CreateFrame("BUTTON", "GHC_UpdateAltInfoButton", GHC.UI.PlayerOptionsFrame, "UIPanelButtonTemplate")
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetPoint("CENTER", 0, 0)
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetText("Configure")
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetSize(80, 22)
-- Standard workaround call OpenToCategory twice https://www.wowinterface.com/forums/showpost.php?p=319664&postcount=2
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetScript("OnClick", function() InterfaceOptionsFrame_OpenToCategory(GHC.addon_name); InterfaceOptionsFrame_OpenToCategory(GHC.addon_name) end)


GHC.UI.GuildBankParentFrame = CreateFrame("SCROLLFRAME", "GHC_GuildBankParentFrame", GuildHelperClassic_BankFrame) --, "TooltipBorderedFrameTemplate")
GHC.UI.GuildBankParentFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.GuildBankParentFrame:SetSize(400, 405)

GHC.UI.GuildBankParentFrame.SendBankDataButton = CreateFrame("BUTTON", "GHC_GuildBankParentFrame_SendBankDataButton", GHC.UI.GuildBankParentFrame, "UIPanelButtonTemplate")
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetPoint("TOPLEFT", 64, 22)
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetSize(80, 22)
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetText("Send Data")
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetScript("OnClick", function() GHC.Functions.SendGuildBankData() end)


GHC.UI.GuildBankParentFrame.MoneyText = GHC.UI.GuildBankParentFrame:CreateFontString("GHC_PlayerOptionsFrame_MoneyTextr", 'OVERLAY', 'GameFontNormal')
GHC.UI.GuildBankParentFrame.MoneyText:SetPoint("TOPRIGHT", 0, 16)
GHC.UI.GuildBankParentFrame.MoneyText:SetText(GetCoinTextureString(0))
GHC.UI.GuildBankParentFrame.MoneyText:SetTextColor(1,1,1,1)
--GHC.UI.GuildBankParentFrame.MoneyText:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))


GHC.UI.GuildBankScrollBar = CreateFrame("Slider", "GHC_GuildBankParentFrame_ScrollFrame", GHC.UI.GuildBankParentFrame, "UIPanelScrollBarTemplate")
GHC.UI.GuildBankScrollBar:SetPoint("TOPLEFT", GHC.UI.GuildBankParentFrame, "TOPRIGHT", -16, -22) 
GHC.UI.GuildBankScrollBar:SetPoint("BOTTOMLEFT", GHC.UI.GuildBankParentFrame, "BOTTOMRIGHT", -16, 8)
GHC.UI.GuildBankScrollBar:SetMinMaxValues(1, 3)
GHC.UI.GuildBankScrollBar:SetValueStep(1.0)
GHC.UI.GuildBankScrollBar.scrollStep = 1
GHC.UI.GuildBankScrollBar:SetValue(1)
GHC.UI.GuildBankScrollBar:SetWidth(16)
GHC.UI.GuildBankScrollBar:SetScript('OnValueChanged', function(self, value) GHC.Functions.UpdateGuildBankGridView(value) end)

GHC.UI.GuildBankGridViewItems = {}
function GHC.UI.DrawGuildBankGridView()
    for row = 1, 11 do
        for col = 1, 10 do
            local f = CreateFrame("FRAME", tostring("GHC_GuildBankGridViewItems_"..row..'_'..col), GHC.UI.GuildBankParentFrame)
            f:SetSize(GHC.UI.GuildBankGridviewIconSize, GHC.UI.GuildBankGridviewIconSize)
            f:SetPoint("TOPLEFT", ((col - 1) * GHC.UI.GuildBankGridviewIconSize + 5 ) + 10, (((row - 1) * GHC.UI.GuildBankGridviewIconSize) + 5) * -1 )

            f.Texture = f:CreateTexture("$parent_Background", "BACKGROUND")
            f.Texture:SetAllPoints(f)

            f.Text = f:CreateFontString("$parent_Text", "OVERLAY", "GameFontNormal")
            f.Text:SetPoint("BOTTOMRIGHT", -4, 4)
            f.Text:SetTextColor(1,1,1,1)
            f.Text:SetFont("Fonts\\FRIZQT__.TTF", GHC.UI.GuildBankGridviewIconTextFontSize)

            f.Item = nil
            f.Link = nil

            f:SetScript("OnEnter", function() GHC.Functions.ShowGuildBankTooltip(f, f.Item) end)
            f:SetScript("OnLeave", function() GameTooltip:Hide() GameTooltip_SetDefaultAnchor(GameTooltip, UIParent) end) -- GHC.UI.GuildBankTooltip:Hide() end)

            table.insert(GHC.UI.GuildBankGridViewItems, f)
        end
    end
end

StaticPopupDialogs["GHC_UpdatePopup"] = {
	text = "There is an update available Guild Helper Classic!", button1 = "Ok",
	timeout = 0, whileDead = true, hideOnEscape = true,	preferredIndex = 3,
}

StaticPopupDialogs["GHC_FixMe"] = {
	text = "To fix any errors you can reset the addon settings, you will need to provide your guild name. |cffC41F3BThis is wipe all data currently stored!|r The name must be an exact match including upper/lower case letters.", button1 = "Ok", button2 = "Cancel",
    hasEditBox = true, EditBoxOnTextChanged = function(self) if self:GetText() ~= '' then self:GetParent().button1:Enable() else self:GetParent().button1:Disable() end end,
	OnShow = function(self) self.editBox:SetText('Guild Name Required!') self.button1:Disable() end,
	OnAccept = function(self) GHC.Functions.Reset(tostring(self.editBox:GetText())) end,
	timeout = 0, whileDead = true, hideOnEscape = true,	preferredIndex = 3,
}

--used for roster listview profession icons
GHC.UI.Tooltip = CreateFrame("FRAME", "GHC_Tooltip", UIParent, "TooltipBorderedFrameTemplate")
GHC.UI.Tooltip.Text = GHC.UI.Tooltip:CreateFontString("GHC_TooltipText", "OVERLAY", "GameFontNormal")
GHC.UI.Tooltip.Text:SetPoint("CENTER", 0, 0)
GHC.UI.Tooltip.Text:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontTooltip))
--GHC.UI.Tooltip.Text:SetTextColor(1,1,1,1)

--this is nasty but until i get time to rewrite the whole addon its what we have
function GHC.UI.LoadNewUI()

	-- local GuildHelperClassic_CloseButton = CreateFrame("BUTTON", "GuildHelperClassic_CloseButton", GuildHelperClassic, "UIPanelButtonTemplate")
	-- GuildHelperClassic_CloseButton:SetPoint("TOPRIGHT", -51, -17)
	-- GuildHelperClassic_CloseButton:SetText('|cffffffffX|r')
	-- GuildHelperClassic_CloseButton:SetSize(29, 22)
	-- GuildHelperClassic_CloseButton:SetScript('OnClick', function() GuildHelperClassic:Hide() end)

end


do
    -- TODO Add ability to show/hide the minimap button from config
    local ldb = LibStub("LibDataBroker-1.1")
    GHC.UI.MinimapButton = ldb:NewDataObject(GHC.addon_name, {
        type = "data source",
        icon = "Interface\\AddOns\\GuildHelperClassic\\GuildHelperClassic",
        -- icon = "Interface\\GuildFrame\\GuildLogo-NoLogo",
        -- icon = "Interface\\GuildFrame\\GuildEmblemsLG_01",
        OnClick = function(self, button)
            if button == "RightButton" then
                -- Standard workaround call OpenToCategory twice
                -- https://www.wowinterface.com/forums/showpost.php?p=319664&postcount=2
                InterfaceOptionsFrame_OpenToCategory(GHC.addon_name)
                InterfaceOptionsFrame_OpenToCategory(GHC.addon_name)
            else
                GHC.UI.Toggle()
            end
        end,
        OnTooltipShow = function(tooltip)
            if not tooltip or not tooltip.AddLine then return end
            tooltip:AddLine(GHC.addon_name)
        end,
    })

    GHC.UI.MinimapIcon = LibStub("LibDBIcon-1.0")
end


function GHC.UI.Toggle()
    if GuildHelperClassic then
        if GuildHelperClassic:IsVisible() then
            GuildHelperClassic:Hide()
        else
            GuildHelperClassic:Show()
        end
    end
end
